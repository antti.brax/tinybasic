package fi.iki.asb.tinybasic;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.Writer;
import java.util.function.Supplier;

public class BasicRuntime {

	private final Program program = new Program();

	private final Variables variables = new Variables();

	private BufferedReader input;

	private Writer output;

	public BasicRuntime(BufferedReader input, Writer output) {
		this.input = input;
		this.output = output;
	}

	public Program getProgram() {
		return program;
	}
	
	public Variables getVariables() {
		return variables;
	}

	public BufferedReader getInput() {
		return input;
	}

	public Writer getOutput() {
		return output;
	}

	// =================================================================== //

	public static Builder readingFrom(Reader input) {
		return new Builder(input);
	}

	public static class Builder implements Supplier<BasicRuntime> {
		private Reader input;
		private Writer output;
		
		public Builder(Reader input) {
			this.input = input;
		}

		public Builder writingTo(Writer output) {
			this.output = output;
			return this;
		}

		public BasicRuntime get() {
			return new BasicRuntime(
					new BufferedReader(input),
					output);
		}
	}
}
