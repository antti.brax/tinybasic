package fi.iki.asb.tinybasic;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import fi.iki.asb.tinybasic.parser.Parser;

public class Main {

	public static void main(String[] args) {
		Parser parser = Parser
				.withRuntime(BasicRuntime
						.readingFrom(new InputStreamReader(System.in))
						.writingTo(new OutputStreamWriter(System.out))
						.get());

		parser.run();
	}

}
