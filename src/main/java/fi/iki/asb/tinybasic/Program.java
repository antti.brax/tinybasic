package fi.iki.asb.tinybasic;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import fi.iki.asb.tinybasic.grammar.NumberedLine;

public class Program {

	private LinkedList<NumberedLine> lines = new LinkedList<>(); 

	private LinkedList<Integer> stack = new LinkedList<>();

	private Integer programCounter = null;

	private Integer nextLine = null;

	public void addLine(NumberedLine line) {
		if (line.getLineNumber() == 0) {
			throw new IllegalStateException("Line number 0 not allowed");
		}

		final ListIterator<NumberedLine> li = lines.listIterator();
		while (li.hasNext()) {
			final NumberedLine nl = li.next();
			if (nl.getLineNumber() == line.getLineNumber()) {
				li.remove();
			} else if (nl.getLineNumber() > line.getLineNumber()) {
				break;
			}
		}

		lines.add(line);
	}

	public List<NumberedLine> getLines() {
		return Collections.unmodifiableList(lines);
	}

	public Integer getProgramCounter() {
		return programCounter;
	}

	public void setProgramCounter(Integer programCounter) {
		this.programCounter = programCounter;
	}

	public void gosub(Integer gosubLine) {
		stack.addFirst(programCounter);
		setNextLine(gosubLine);
	}

	public void gosubReturn() {
		if (stack.isEmpty()) {
			throw new IllegalStateException("RETURN has no matching GOSUB");
		} else {
			programCounter = stack.removeFirst();
		}
	}

	public void setNextLine(Integer nextLine) {
		this.nextLine = nextLine;
	}

	public NumberedLine advanceProgramCounter() {
		if (lines.isEmpty()) {
			throw new IllegalStateException("RUN with no program in memory");
		}

		final ListIterator<NumberedLine> li = lines.listIterator();
		while (li.hasNext()) {
			final NumberedLine nl = li.next();
			
			if (nextLine != null) {
				if (nl.getLineNumber().intValue() == nextLine) {
					programCounter = nl.getLineNumber();
					nextLine = null;
					return nl;
				}
			} else {
				if (nl.getLineNumber().intValue() > programCounter) {
					programCounter = nl.getLineNumber();
					return nl;
				}
			}
		}
		
		if (nextLine != null) {
			nextLine = null;
			throw new IllegalStateException("No line to GO TO");
		} else {
			throw new IllegalStateException("No END");
		}
	}
}
