package fi.iki.asb.tinybasic;

import java.util.HashMap;

public class Variables {

	private HashMap<String, Integer> variables = new HashMap<>();

	public void setVariable(String var, Integer val) {
		variables.put(var, val);
	}

	public Integer getVariable(String var) {
		Integer val = variables.get(var);
		
		if (val == null) {
			return 0;
		} else {
			return val;
		}
	}

}
