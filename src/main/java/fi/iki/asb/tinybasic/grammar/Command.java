package fi.iki.asb.tinybasic.grammar;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class Command {

	private Line originalLine;

	public Command(Line originalLine) {
		this.originalLine = originalLine;
	}

	public void execute(BasicRuntime runtime) {
		originalLine.reset();
		final Number lineNumber = Number.readNumber(originalLine);
		final Statement statement = Statement.readStatement(originalLine.copyRemaining());

		if (lineNumber != null) {
			runtime.getProgram().addLine(NumberedLine
					.makeNumberedLine(
							originalLine,
							lineNumber.getNumber(),
							statement));
		} else {
			statement.execute(runtime);
		}
	}

	public static Command readCommand(Line str) {
		return new Command(str);
	}
}
