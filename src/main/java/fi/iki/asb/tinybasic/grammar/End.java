package fi.iki.asb.tinybasic.grammar;

import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.Program;
import fi.iki.asb.tinybasic.parser.Line;

public class End implements Consumer<BasicRuntime> {
	
	private Line followingKeyword;

	private End(Line followingKeyword) {
		this.followingKeyword = followingKeyword;
	}

	@Override
	public void accept(BasicRuntime t) {
		followingKeyword.reset().mark().skip(c -> c == ' ');
		if (followingKeyword.hasNext()) {
			throw new IllegalArgumentException();
		}

		final Program program = t.getProgram();
		if (program.getProgramCounter() == null) {
			throw new IllegalStateException("Program is not running");
		}
		
		program.setProgramCounter(null);
	}

	public static Consumer<BasicRuntime> readEnd(Line line) {
		return new End(line.copyRemaining());
	}
}
