package fi.iki.asb.tinybasic.grammar;

import static fi.iki.asb.tinybasic.parser.LineUtil.skipSpaces;

import java.util.function.Function;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class Expression implements Function<BasicRuntime, Integer> {

	private final Line line;

	private Function<BasicRuntime, Integer> parsedExpression = null;

	private Expression(Line line) {
		this.line = line;
	}

	@Override
	public Integer apply(BasicRuntime runtime) {
		parse();
		return parsedExpression.apply(runtime);
	}

	public void parse() {
		if (parsedExpression == null) {
			parsedExpression = parseExpression();
		}
	}

	private Function<BasicRuntime, Integer> parseExpression() {
		return parseAddSub();
	}

	private Function<BasicRuntime, Integer> parseAddSub() {
		Function<BasicRuntime, Integer> l;
		Function<BasicRuntime, Integer> r;

		l = parseMulDiv();
		
		while (true) {
			skipSpaces(line);
			if (! line.hasNext() || (line.peek() != '+' && line.peek() != '-')) {
				return l;
			}

			final char ch = line.next();
			
			r = parseMulDiv();

			if (ch == '+') {
				return rt -> l.apply(rt)  + r.apply(rt);
			} else {
				return rt -> l.apply(rt) - r.apply(rt);				
			}
		}
	}

	private Function<BasicRuntime, Integer> parseMulDiv() {
		Function<BasicRuntime, Integer> l;
		Function<BasicRuntime, Integer> r;

		l = parseOperands();

		while (true) {
			skipSpaces(line);
			if (! line.hasNext() || (line.peek() != '*' && line.peek() != '/')) {
				return l;
			}

			final char ch = line.next();
			
			r = parseOperands();
			
			if (ch == '*') {
				return rt -> l.apply(rt) * r.apply(rt);
			} else {
				return rt -> l.apply(rt) / r.apply(rt);
			}
		}
	}

	private Function<BasicRuntime, Integer> parseOperands() {
		skipSpaces(line);

		if (line.skip("(")) {
			final Function<BasicRuntime, Integer>  v = parseAddSub();

			skipSpaces(line);
			if (! line.skip(")")) {
				throw new IllegalStateException("Syntax error - expects \")\"");
			}

			return v;
		}

		Number number = Number.readNumber(line);
		if (number != null) {
			return rt -> number.getNumber();
		}
		
		Var var = Var.readVar(line);
		if (var != null) {
			return var;
		}

		throw new IllegalStateException("Syntax error in expression - expects value");
	}

	public static Expression readExpression(Line line) {
		return new Expression(line);
	}
}
