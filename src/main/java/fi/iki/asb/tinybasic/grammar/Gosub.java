package fi.iki.asb.tinybasic.grammar;

import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class Gosub implements Consumer<BasicRuntime> {

	private final Expression expression;

	private Gosub(Line line) {
		this.expression = Expression.readExpression(line);
	}

	@Override
	public void accept(BasicRuntime runtime) {
		final Integer lineNumber = expression.apply(runtime);
		runtime.getProgram().gosub(lineNumber);
	}

	public static Consumer<BasicRuntime> readGosub(Line line) {
		return new Gosub(line);
	}
}
