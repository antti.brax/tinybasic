package fi.iki.asb.tinybasic.grammar;

import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class Goto implements Consumer<BasicRuntime> {

	private final Expression expression;

	private Goto(Line line) {
		this.expression = Expression.readExpression(line);
	}

	@Override
	public void accept(BasicRuntime runtime) {
		final Integer lineNumber = expression.apply(runtime);
		runtime.getProgram().setNextLine(lineNumber);
	}

	public static Consumer<BasicRuntime> readGoto(Line line) {
		return new Goto(line);
	}
}
