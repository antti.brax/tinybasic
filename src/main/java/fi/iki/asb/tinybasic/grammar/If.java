package fi.iki.asb.tinybasic.grammar;

import static fi.iki.asb.tinybasic.parser.LineUtil.skipSpaces;

import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class If implements Consumer<BasicRuntime> {
	
	private enum Relop {
		EQ,
		LT,
		GT,
		LE,
		GE,
		NE
	}

	private final Line line;

	private Consumer<BasicRuntime> parsedIf;

	private If(Line line) {
		this.line = line;
	}

	@Override
	public void accept(BasicRuntime runtime) {
		if (parsedIf == null) {
			parsedIf = parseIf();
		}

		parsedIf.accept(runtime);
	}
	
	private Consumer<BasicRuntime> parseIf() {
		Expression l = Expression.readExpression(line);
		l.parse();

		skipSpaces(line);

		Relop relop;
		
		if (line.skip("=")) {
			relop = Relop.EQ;
		} else if (line.skip("<=")) {
			relop = Relop.LE;
		} else if (line.skip(">=")) {
			relop = Relop.GE;
		} else if (line.skip("<>")) {
			relop = Relop.NE;
		} else if (line.skip("<")) {
			relop = Relop.LT;
		} else if (line.skip(">")) {
			relop = Relop.GT;
		} else {
			throw new IllegalStateException("IF expects a relative operation");
		}

		Expression r = Expression.readExpression(line);
		r.parse();

		skipSpaces(line);
		if (! line.skip("THEN")) {
			throw new IllegalStateException("IF expects THEN");
		}

		Statement statement = Statement.readStatement(line.copyRemaining());

		switch (relop) {
		case EQ:
			return rt -> { if (l.apply(rt) == r.apply(rt)) statement.execute(rt); };
		case GE:
			return rt -> { if (l.apply(rt) >= r.apply(rt)) statement.execute(rt); };
		case GT:
			return rt -> { if (l.apply(rt) > r.apply(rt)) statement.execute(rt); };
		case LE:
			return rt -> { if (l.apply(rt) <= r.apply(rt)) statement.execute(rt); };
		case LT:
			return rt -> { if (l.apply(rt) < r.apply(rt)) statement.execute(rt); };
		case NE:
			return rt -> { if (l.apply(rt) != r.apply(rt)) statement.execute(rt); };
		default:
			throw new IllegalStateException("Illegal state");
		}
	}

	public static Consumer<BasicRuntime> readIf(Line line) {
		return new If(line);
	}
}
