package fi.iki.asb.tinybasic.grammar;

import static fi.iki.asb.tinybasic.parser.LineUtil.skipSpaces;
import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class Let implements Consumer<BasicRuntime> {

	private final Line line;
	private Consumer<BasicRuntime> parsedLet;

	private Let(Line line) {
		this.line = line;
	}

	@Override
	public void accept(BasicRuntime runtime) {
		if (parsedLet == null) {
			parsedLet = parseLet();
		}

		parsedLet.accept(runtime);
	}
	
	private Consumer<BasicRuntime> parseLet() {
		final Var var = Var.readVar(line);
		if (var == null) {
			return rt -> { throw new IllegalStateException("LET is missing a variable name"); };
		}

		skipSpaces(line);
		if (line.peek() != '=') {
			return rt -> { throw new IllegalStateException("LET is missing an ="); };
		}
		line.next();
		
		final Expression expression = Expression.readExpression(line.copyRemaining());

		return rt -> {
			rt.getVariables().setVariable(var.getName(), expression.apply(rt));
		};
	}

	public static Consumer<BasicRuntime> readLet(Line line) {
		return new Let(line.copyRemaining());
	}
}
