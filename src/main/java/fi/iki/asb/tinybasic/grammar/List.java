package fi.iki.asb.tinybasic.grammar;

import java.io.IOException;
import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.Program;
import fi.iki.asb.tinybasic.parser.Line;

public class List implements Consumer<BasicRuntime> {
	
	private Line followingKeyword;

	private List(Line followingKeyword) {
		this.followingKeyword = followingKeyword;
	}

	@Override
	public void accept(BasicRuntime runtime) {
		followingKeyword.reset().mark().skip(c -> c == ' ');
		if (followingKeyword.hasNext()) {
			throw new IllegalArgumentException();
		}

		final Program program = runtime.getProgram();
		for (NumberedLine line: program.getLines()) {
			try {
				runtime.getOutput().write(line.getOriginalLine().toString());
				runtime.getOutput().write("\n");
				runtime.getOutput().flush();
			} catch (IOException ex) {
				throw new IllegalStateException("Write failed", ex);
			}
		}
	}

	public static Consumer<BasicRuntime> readList(Line line) {
		return new List(line.copyRemaining());
	}
}
