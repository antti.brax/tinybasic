package fi.iki.asb.tinybasic.grammar;

import static fi.iki.asb.tinybasic.parser.LineUtil.skipSpaces;

import fi.iki.asb.tinybasic.parser.Line;

public class Number {
	
	private final int n;

	public Number(int n) {
		this.n = n;
	}

	public int getNumber() {
		return n;
	}

	public static Number readNumber(Line str) {
		skipSpaces(str);

		str.mark();
		if (str.skip(c -> c == '+' || c == '-') > 1) {
			str.rewind();
			return null;
		}

		str.skip(Character::isDigit);

		Line marked = str.copyMarked();
		if (marked.length() == 0) {
			str.rewind();
			return null;
		} else {
			return new Number(Integer.parseInt(marked.toString()));
		}
	}
}
