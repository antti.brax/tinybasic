package fi.iki.asb.tinybasic.grammar;

import fi.iki.asb.tinybasic.parser.Line;

public class NumberedLine {

	private final Line originalLine;

	private final Integer lineNumber;

	private final Statement statement;

	private NumberedLine(
			final Line originalLine,
			final Integer lineNumber,
			final Statement statement) {
		this.originalLine = originalLine;
		this.lineNumber = lineNumber;
		this.statement = statement;
	}
	
	public Integer getLineNumber() {
		return lineNumber;
	}

	public Line getOriginalLine() {
		return originalLine;
	}
	
	public Statement getStatement() {
		return statement;
	}

	public static NumberedLine makeNumberedLine(Line originalLine, Integer lineNumber, Statement statement) {
		return new NumberedLine(originalLine, lineNumber, statement);
	}
}
