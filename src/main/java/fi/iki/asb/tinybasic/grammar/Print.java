package fi.iki.asb.tinybasic.grammar;

import static fi.iki.asb.tinybasic.parser.LineUtil.skipSpaces;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.function.Function;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class Print implements Consumer<BasicRuntime> {

	private final Line line;
	
	private Collection<Function<BasicRuntime, String>> printlist = null;

	private Print(Line line) {
		this.line = line;
	}

	@Override
	public void accept(BasicRuntime runtime) {
		if (printlist == null) {
			printlist = parsePrintlist();
		}

		try {
			for (Function<BasicRuntime, String> printitem: printlist) {
				runtime.getOutput().write(printitem.apply(runtime));
				runtime.getOutput().flush();
			}
		} catch (IOException e) {
			throw new IllegalStateException("IO error", e);
		}
	}

	private Collection<Function<BasicRuntime, String>> parsePrintlist() {
		Collection<Function<BasicRuntime, String>> p = new ArrayList<>();

		while (true) {
			Function<BasicRuntime, String> printitem = parsePrintitem();
			if (printitem == null)  {
				break;
			}
			
			p.add(printitem);

			skipSpaces(line);
			if (! line.hasNext()) {
				p.add(rt -> "\r\n");
				break;
			} else if (line.skip(";")) {
				// NOP
			} else if (line.skip(",")) {
				p.add(rt -> " ");
			}
		}
		
		if (p.isEmpty()) {
			p.add(rt -> "\r\n");
		}

		return p;
	}

	private Function<BasicRuntime, String> parsePrintitem() {
		skipSpaces(line);
		if (! line.hasNext()) {
			return null;
		}

		if (line.peek() == '"') {
			return parseCharacterstring();
		} else {
			return parseExpression();
		}
	}

	private Function<BasicRuntime, String> parseCharacterstring() {
		line.mark();
		line.next(); // Skip first colon.
		line.skip(ch -> ch != '"');
		
		if (line.hasNext()) {
			line.next();
			String str = line.copyMarked().toString();
			return rt -> str.substring(1, str.length() - 1);
		} else {
			throw new IllegalStateException("Missing close quote in PRINT string");
		}
	}

	private Function<BasicRuntime, String> parseExpression() {
		Expression exp = Expression.readExpression(line);
		exp.parse();
		return rt -> exp.apply(rt).toString();
	}


	public static Consumer<BasicRuntime> readPrint(Line line) {
		return new Print(line.copyRemaining());
	}
}
