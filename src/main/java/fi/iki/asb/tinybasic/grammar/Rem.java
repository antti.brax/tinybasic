package fi.iki.asb.tinybasic.grammar;

import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class Rem implements Consumer<BasicRuntime> {
	
	private static final Rem INSTANCE = new Rem();

	private Rem() {
		// NOP
	}

	@Override
	public void accept(BasicRuntime t) {
		// NOP
	}

	public static Consumer<BasicRuntime> readRem(Line line) {
		return INSTANCE;
	}
}
