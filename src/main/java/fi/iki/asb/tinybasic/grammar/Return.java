package fi.iki.asb.tinybasic.grammar;

import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.Program;
import fi.iki.asb.tinybasic.parser.Line;

public class Return implements Consumer<BasicRuntime> {
	
	private Line followingKeyword;

	private Return(Line followingKeyword) {
		this.followingKeyword = followingKeyword;
	}

	@Override
	public void accept(BasicRuntime t) {
		followingKeyword.reset().mark().skip(c -> c == ' ');
		if (followingKeyword.hasNext()) {
			throw new IllegalArgumentException("Unexpected characters after RETURN");
		}

		final Program program = t.getProgram();
		if (program.getProgramCounter() == null) {
			throw new IllegalStateException("Program is not running");
		}

		program.gosubReturn();
	}

	public static Consumer<BasicRuntime> readReturn(Line line) {
		return new Return(line);
	}
}
