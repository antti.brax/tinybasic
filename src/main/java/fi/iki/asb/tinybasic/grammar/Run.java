package fi.iki.asb.tinybasic.grammar;

import java.util.List;
import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.Program;
import fi.iki.asb.tinybasic.parser.Line;

public class Run implements Consumer<BasicRuntime> {
	
	private Line followingKeyword;
	
	public Run(Line followingKeyword) {
		this.followingKeyword = followingKeyword;
	}

	@Override
	public void accept(BasicRuntime runtime) {
		followingKeyword.reset().mark().skip(c -> c == ' ');
		if (followingKeyword.hasNext()) {
			throw new IllegalArgumentException();
		}

		final Program program = runtime.getProgram();
		if (program.getProgramCounter() != null) {
			throw new IllegalStateException("Program is already running");
		}

		final List<NumberedLine> lines = program.getLines();
		if (lines.isEmpty()) {
			throw new IllegalStateException("RUN with no program in memory");
		}

		program.setProgramCounter(0);
		while (program.getProgramCounter() != null) {
			NumberedLine currentLine = program.advanceProgramCounter();
			Statement statement = currentLine.getStatement();

			try {
				statement.execute(runtime);
			} catch (RuntimeException e1) {
				program.setProgramCounter(null);
				throw new IllegalStateException(
						currentLine.getLineNumber() + ": " + e1.getMessage(),
						e1);
			}
		}
	}

	public static Consumer<BasicRuntime> readRun(Line line) {
		return new Run(line.copyRemaining());
	}
}
