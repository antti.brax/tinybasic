package fi.iki.asb.tinybasic.grammar;

import java.util.function.Consumer;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class Statement {

	private final Line line;

	private Consumer<BasicRuntime> parsedStatement = null;

	private Statement(Line line) {
		this.line = line;
	}

	public void execute(BasicRuntime runtime) {
		if (parsedStatement == null) {
			parsedStatement = parseStatement();
		}

		parsedStatement.accept(runtime);
	}

	private Consumer<BasicRuntime> parseStatement() {
		line.reset()
			.skip(c -> c == ' ');

		// Empty line.
		if (! line.hasNext()) {
			return rt -> { /* NOP */ };
		}

		if (line.skip("END")) {
			return End.readEnd(line);
		}

		if (line.skip("GOSUB")) {
			return Gosub.readGosub(line.copyRemaining());
		}

		if (line.skip("GOTO")) {
			return Goto.readGoto(line.copyRemaining());
		}

		if (line.skip("IF")) {
			return If.readIf(line.copyRemaining());
		}

		if (line.skip("LET")) {
			return Let.readLet(line);
		}

		if (line.skip("LIST")) {
			return List.readList(line);
		}

		if (line.skip("PRINT")) {
			return Print.readPrint(line);
		}

		if (line.skip("REM")) {
			return Rem.readRem(line);
		}

		if (line.skip("RETURN")) {
			return Return.readReturn(line.copyRemaining());
		}

		if (line.skip("RUN")) {
			return Run.readRun(line);
		}

		throw new UnsupportedOperationException("Not implemented yet");
	}

	public static Statement readStatement(Line str) {
		return new Statement(str);
	}
}
