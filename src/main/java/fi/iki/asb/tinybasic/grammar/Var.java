package fi.iki.asb.tinybasic.grammar;

import static fi.iki.asb.tinybasic.parser.LineUtil.skipSpaces;

import java.util.function.Function;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.parser.Line;

public class Var implements Function<BasicRuntime, Integer> {

	private final String name;

	public Var(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public Integer apply(BasicRuntime runtime) {
		return runtime.getVariables().getVariable(name);
	}

	public static Var readVar(Line line) {
		skipSpaces(line);

		line.mark();
		char ch = line.peek();

		if (ch < 'A' || ch > 'Z') {
			return null;
		}
		
		line.next();
		return new Var(String.valueOf(ch));
	}
}
