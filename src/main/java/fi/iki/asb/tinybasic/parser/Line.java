package fi.iki.asb.tinybasic.parser;

import java.util.function.Predicate;

public class Line {

	private String s;
	private int index;
	private int mark = -1;

	public Line(String s) {
		this.s = s;
	}
	
	public int length() {
		return s.length();
	}
	
	@Override
	public String toString() {
		return s;
	}

	public char peek() {
		return s.charAt(index);
	}

	public boolean hasNext() {
		return index < s.length();
	}

	public char next() {
		return s.charAt(index++);
	}
	
	public Line mark() {
		mark = index;
		return this;
	}

	public void rewind() {
		if (mark == -1) {
			throw new IllegalStateException();
		}

		index = mark;
	}

	public Line skipAll() {
		index = s.length();
		return this;
	}

	public Line reset() {
		index = 0;
		mark = -1;
		return this;
	}

	public int skip(Predicate<Character> predicate) {
		int skipped = 0;
		while (hasNext() && predicate.test(peek())) {
			skipped++;
			next();
		}
		return skipped;
	}

	public boolean skip(String prefix) {
		mark();
		for (char c: prefix.toCharArray()) {
			if (hasNext() && peek() == c) {
				next();
			} else {
				rewind();
				return false;
			}
		}

		return true;
	}

	public Line copyRemaining() {
		return new Line(s.substring(index));
	}

	public Line copyMarked() {
		return new Line(s.substring(mark, index));
	}
}
