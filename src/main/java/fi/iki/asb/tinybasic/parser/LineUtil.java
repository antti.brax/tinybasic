package fi.iki.asb.tinybasic.parser;

public class LineUtil {

	public static int skipSpaces(Line line) {
		return line.skip(c -> c == ' ');
	}

}
