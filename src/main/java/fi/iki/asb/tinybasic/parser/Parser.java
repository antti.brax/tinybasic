package fi.iki.asb.tinybasic.parser;

import java.io.IOException;

import fi.iki.asb.tinybasic.BasicRuntime;
import fi.iki.asb.tinybasic.grammar.Command;

public class Parser implements Runnable {

	private final BasicRuntime runtime;

	private Parser(BasicRuntime runtime) {
		this.runtime = runtime;
	}

	public static Parser withRuntime(BasicRuntime runtime) {
		return new Parser(runtime); 
	}

	// =================================================================== //

	@Override
	public void run() {
		while (true) {
			try {
				final String input = runtime.getInput().readLine();
				if (input == null) {
					return;
				}

				try {
					Command command = Command.readCommand(new Line(input));
					command.execute(runtime);
				} catch (RuntimeException e1) {
					runtime.getOutput().write(e1.getMessage());
					runtime.getOutput().write("\n");
					runtime.getOutput().flush();

					e1.printStackTrace();
				}

			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
