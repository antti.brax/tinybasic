package fi.iki.asb.tinybasic;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.StringWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fi.iki.asb.tinybasic.parser.Parser;

public class ParserTest {

	private PipedWriter input;
	private StringWriter output; 

	/**
	 * Parser reads from this.
	 */
	private PipedReader sink;

	private BasicRuntime runtime;
	
	private Parser parser;
	
	private Thread thread;

	@Before
	public void setUp() throws IOException, InterruptedException {
		sink = new PipedReader();
		input = new PipedWriter(sink);
		output = new StringWriter();

		runtime = BasicRuntime
				.readingFrom(sink)
				.writingTo(output)
				.get();

		parser = Parser
				.withRuntime(runtime);
		
		thread = new Thread(parser);
		thread.start();
	}
	
	@After
	public void tearDown() throws IOException {
		sink.close();
	}

	@Test
	public void print_stringLiteralShouldBePrinted() throws IOException, InterruptedException {
		input.write("10 PRINT \"A\"\n");
		input.write("20 END\n");
		input.write("RUN\n");
		input.close();
		thread.join();
		
		assertEquals("A\r\n", output.toString());
	}

	@Test
	public void print_trailingColonShouldResultInNoNewline() throws IOException, InterruptedException {
		input.write("10 PRINT \"A\";\n");
		input.write("20 END\n");
		input.write("RUN\n");
		input.close();
		thread.join();
		
		assertEquals("A", output.toString());
	}

	@Test
	public void print_trailingCommaShouldResultInSpaceAndNoNewline() throws IOException, InterruptedException {
		input.write("10 PRINT \"A\",\n");
		input.write("20 END\n");
		input.write("RUN\n");
		input.close();
		thread.join();
		
		assertEquals("A ", output.toString());
	}

	@Test
	public void print_commaShouldResultInSpace() throws IOException, InterruptedException {
		input.write("10 PRINT \"A\",\"B\"\n");
		input.write("20 END\n");
		input.write("RUN\n");
		input.close();
		thread.join();
		
		assertEquals("A B\r\n", output.toString());
	}

	@Test
	public void print_colonShouldResultInNoSpace() throws IOException, InterruptedException {
		input.write("10 PRINT \"A\";\"B\"\n");
		input.write("20 END\n");
		input.write("RUN\n");
		input.close();
		thread.join();
		
		assertEquals("AB\r\n", output.toString());
	}

	@Test
	public void print_variableShouldPrint() throws IOException, InterruptedException {
		input.write("10 LET A = 10\n");
		input.write("20 PRINT A\n");
		input.write("30 END\n");
		input.write("RUN\n");
		input.close();
		thread.join();
		
		assertEquals("10\r\n", output.toString());
	}

	@Test
	public void print_expressionShouldPrint() throws IOException, InterruptedException {
		input.write("10 PRINT 20 * 30\n");
		input.write("20 END\n");
		input.write("RUN\n");
		input.close();
		thread.join();
		
		assertEquals("600\r\n", output.toString());
	}

	@Test
	public void print_ifLessThanIsTrue() throws IOException, InterruptedException {
		run(    "10 IF 1 < 2 THEN GOTO 60",
				"20 GOTO 40",
				"30 END",
				"40 PRINT \"false\";",
				"50 END",
				"60 PRINT \"true\";",
				"70 END");

		assertEquals("true", output.toString());
	}

	@Test
	public void print_ifLessThanIsFalse() throws IOException, InterruptedException {
		run(    "10 IF 2 < 1 THEN GOTO 60",
				"20 GOTO 40",
				"30 END",
				"40 PRINT \"false\";",
				"50 END",
				"60 PRINT \"true\";",
				"70 END");

		assertEquals("false", output.toString());
	}

	@Test
	public void print_ifGreaterThanIsTrue() throws IOException, InterruptedException {
		run(    "10 IF 2 > 1 THEN GOTO 60",
				"20 GOTO 40",
				"30 END",
				"40 PRINT \"false\";",
				"50 END",
				"60 PRINT \"true\";",
				"70 END");

		assertEquals("true", output.toString());
	}

	@Test
	public void print_ifGreaterThanIsFalse() throws IOException, InterruptedException {
		run(    "10 IF 1 > 2 THEN GOTO 60",
				"20 GOTO 40",
				"30 END",
				"40 PRINT \"false\";",
				"50 END",
				"60 PRINT \"true\";",
				"70 END");
		
		assertEquals("false", output.toString());
	}

	@Test
	public void print_ifEqualIsTrueWithVariable() throws IOException, InterruptedException {
		run(    "10 LET A = 10",
				"20 IF A = 10 THEN GOTO 70",
				"30 GOTO 50",
				"40 END",
				"50 PRINT \"false\";",
				"60 END",
				"70 PRINT \"true\";",
				"80 END");

		assertEquals("true", output.toString());
	}

	@Test
	public void print_ifEqualIsFalseWithVariable() throws IOException, InterruptedException {
		run(    "10 LET A = 10",
				"15 LET B = 20",
				"20 IF A = B THEN GOTO 70",
				"30 GOTO 50",
				"40 END",
				"50 PRINT \"false\";",
				"60 END",
				"70 PRINT \"true\";",
				"80 END");

		assertEquals("false", output.toString());
	}

	@Test
	public void print_ifEqualIsTrueWithExpression() throws IOException, InterruptedException {
		run(    "10 LET A = 20",
				"20 IF A = 10 + 5 * 2 THEN GOTO 70",
				"30 GOTO 50",
				"40 END",
				"50 PRINT \"false\";",
				"60 END",
				"70 PRINT \"true\";",
				"80 END");

		assertEquals("true", output.toString());
	}

	@Test
	public void print_ifEqualIsFalseWithExpression() throws IOException, InterruptedException {
		run(    "20 IF 2 * 3 = 10 + 5 * 2 THEN GOTO 70",
				"30 GOTO 50",
				"40 END",
				"50 PRINT \"false\";",
				"60 END",
				"70 PRINT \"true\";",
				"80 END");

		assertEquals("false", output.toString());
	}

	@Test
	public void print_loop() throws IOException, InterruptedException {
		run(    "10 LET A = 0",
				"20 IF A >= 10 THEN GOTO 60",
				"30   PRINT A;",
				"40   LET A = A + 1",
				"50   GOTO 20",
				"60 END");

		assertEquals("0123456789", output.toString());
	}

	@Test
	public void print_gotoToVariable() throws IOException, InterruptedException {
		run(    "10 LET R = 30",
				"20 GOTO 100",
				"30 END",
				"100 PRINT \"sub\";",
				"110 GOTO R");

		assertEquals("sub", output.toString());
	}

	private void run(String ... lines) throws IOException, InterruptedException {
		for (String line: lines) {
			input.write(line);
			input.write("\n");
		}
		input.write("RUN\n");
		input.close();
		thread.join();
	}
}
